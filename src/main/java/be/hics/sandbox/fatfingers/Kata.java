package be.hics.sandbox.fatfingers;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Kata {

    public static String fatFingers(String str) {
        if (Objects.isNull(str)) return null;
        String[] parts = str.split("(?i)a");
        Function<Character, String> reverseCaseChar = c -> {
            if (Character.isLetter(c))
                return Character.isUpperCase(c) ? String.valueOf(c).toLowerCase() : String.valueOf(c).toUpperCase();
            return String.valueOf(c);
        };
        IntFunction<String> reverseCaseInt = i -> reverseCaseChar.apply((char) i);
        IntFunction<String> reverseCaseParts = i -> i % 2 == 0 ? parts[i] : parts[i].chars().mapToObj(reverseCaseInt).collect(Collectors.joining());
        return IntStream.range(0, parts.length).mapToObj(reverseCaseParts).collect(Collectors.joining());
    }

//    public static String fatFingersImperative(String str) {
//        if (Objects.isNull(str))
//            return null;
//        String result = "";
//        int f = 0, t;
//        for (int c : str.toCharArray())
//            if ((t = c & 95) == 65) f ^= 1;
//            else result += String.format("%c", f < 1 | t < 66 | t > 90 ? c : c ^ 32);
//        return result;
//    }

}