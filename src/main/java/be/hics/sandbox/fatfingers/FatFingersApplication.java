package be.hics.sandbox.fatfingers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FatFingersApplication {

	public static void main(String[] args) {
		SpringApplication.run(FatFingersApplication.class, args);
		fatFingersPrettyPrint("The quick brown fox jumps over the lazy dog.");
		fatFingersPrettyPrint("aAaaaaAaaaAAaAa");
		fatFingersPrettyPrint("The end of the institution, maintenance, and administration of government, is to secure the existence of the body politic, to protect it, and to furnish the individuals who compose it with the power of enjoying in safety and tranquillity their natural rights, and the blessings of life: and whenever these great objects are not obtained, the people have a right to alter the government, and to take measures necessary for their safety, prosperity and happiness.");
	}

	private static void fatFingersPrettyPrint(final String str) {
        System.out.println(str);
        System.out.println(Kata.fatFingers(str));
        System.out.println("---");
    }
}
